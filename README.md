Simple file to read json stubs
======

The library include an object with 2 methods:

1. stubReader
2. [stubReaderSync](#stubReaderSync)


<a name="stubReaderSync"></a>
# Method `stubReaderSync`
The `stubReaderSync` accept 2 params ([path](#path) and [options](#options)) and return an array of [FileInfo](#FileInfo).

<a name="path"></a>
# path {String}

Path to the main folder where all the stubs are included (the method will check also in the sub-folders).

<a name="options"></a>
# options {object}

Options for the transformation, params acepted:

## apiNameMapper {Function}
Acept a `String` as input param, and return a `String`.

<a name="FileInfo"></a>
# FileInfo {Object}

Inclue several attributes: [apiInfo](#apiInfo), [originalApiName](#originalApiName), [fileName](#fileName), [api](#api), [content](#content)

<a name="apiInfo"></a>
## apiInfo {String}

Includes

<a name="originalApiName"></a>
## originalApiName {String}

<a name="fileName"></a>
## fileName {String}

The complete name of the file

<a name="api"></a>
## api {String}

<a name="content"></a>
## content {String}

The content of the file

How to use
=====

1. Install it via npm `npm i -S https://gitlab.com/raffaele/stub-reader.git`.
2. Include in your file: `const reader = require('stub-reader');`
3. Execute the function with the path of your stub folder: `const results = reader.stubReaderSync('path-to-stub-folder');`
4. You can verify the content of `results` is an array of [FileInfo](#FileInfo). Every element is referred to a different json file in the stub folder.

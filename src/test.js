const reader = require('./reader'),
    path = require('path');

const stubsRoot = path.join(__dirname, '../stubs');

const result = reader.stubReaderSync(stubsRoot);
console.log(result);

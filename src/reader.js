const fs = require('fs'),
    path = require('path');

module.exports = {
    stubReader: stubReader,
    stubReaderSync: stubReaderSync
};

class FileInfo {
    constructor (apiInfo, originalApiName, api) {
        const apiInfoPath = apiInfo.path,
            content = fs.readFileSync(apiInfoPath, 'utf8');

        this.apiInfo = apiInfo;
        this.originalApi = originalApiName;
        this.fileName = path.basename(apiInfoPath);
        this.api = api;
        this.content = JSON.parse(content);
    }
}

function defaultApiNameMapper (apiName) {
    return apiName.replace(/\-[a-z0-9]/g, function (singleMatch) {
        return singleMatch.substr(1).toUpperCase();
    });
}

function stubReaderSync (rootPath, options) {
    const root = path.resolve(rootPath),
        apiList = getThree(root);

    const defaultOptions = {
        apiNameMapper: defaultApiNameMapper
    };
    
    options = Object.assign(defaultOptions, options);
    

    return apiList.map(apiInfo => {
        const originalApiName = path.relative(root, apiInfo.path),
            api = options.apiNameMapper(originalApiName);
            
        return new FileInfo(apiInfo, originalApiName, api);
    });
}

function getThree (absPath) {
    const layerResults = fs.readdirSync(absPath, {encoding: 'utf-8'});
    const completePath = layerResults.reduce((prev, actual) => {
        const finalPath = path.join(absPath, actual),
            fileStat = fs.statSync(finalPath);

        if (fileStat.isFile() && finalPath.match(/\.json$/)) {
            prev.push({
                path: finalPath
            });
        } else if (fileStat.isDirectory()) {
            prev = prev.concat(getThree(finalPath));
        }
        
        return prev;
    }, []);
    
    return completePath;
}

function stubReader (path, options, callback) {
    options = options || {};
    setTimeout(()=>{
        try {
            const result = stubReaderSync(path, options);
            callback(null, result);
        } catch (ex) {
            return callback(ex);
        }

    });
}

// const result = stubReaderSync('./stubs');

// console.log('result', result);
